# Notes for play
---
## TODO
- [ ] Change DOSC and DOSE to something ~~cooler sounding~~ more ominus
- [ ] Emtional depth for Nataliya
- [x] ~~CONGO should be a clever acronym~~ delete CONGOO

## General note
- I don't think we need any actual individuals for CONGO. It's mostly a plot point anyways
- Play should carry veil of significance that drops at end (Nataliya's death)
- Gustavo is on fence about Nataliya taking the risk she is
---
## Notebook details
- Mind reading tech inspired by [this](https://www.cmu.edu/dietrich/news/news-stories/2017/june/brain-decoding-complex-thoughts.html)
- Story version is miniturized (roughly pea-sized) and portable
- Plan is to begin embedding the tech in all electronics by pressuring manufacturers
- Mind reading tech gradually replaces legacy, non-mind-reading tech until every electronic device can read minds
- State then begins to assasinate people for even thinking about going against the state
---
## State details
- State is insipred by modern North Korea mixed with America.
- State kills any who even speak against it
- State enforces random, pointless rituals in order to ensure people are willing to be compliant without understanding why
- State controls all means of distributing information except word of mouth.
- State has police who pose as citizens to try to intercept word of mouth
- People are able to easily identify police due to manerisms that are inconsitent with the rest of society
- Actual year play is set in is 2140, but nobody in universe actualy knows what year it is. Officially the year is 2000 according to the state.
- Play takes place on December 25th, 2140 in universe (June 30, 2000 according to the state)
